# React test

Starting point:
https://www.npmjs.com/package/generator-react-fullstack

- Nice developer environment
- Jest tests won't run with Node versions > 0.10.36, without upding jest to 0.5.x and using io.js
- Needs some of the packeges being updated before starting testing

## What to do differently

Current folder structure follows Flux pattern. I would prefer structuring application in associative way. So, that everything related to same context is in same folder e.g:

- Article related information is in article -folder
  - Article.js
  - ArticleList.js
  - ArticleListPage.js
  - ArticleActions.js
  - ArticleStore.js
  - ArticleApi.js
  - __tests__
    - ArticleStore-test.js
    - ArticleApi-test.js
- core
  - Dispatcher.js
  - utils.js
- app.js

## Requirements
- users can see a list of articles
- users can press a button to load more articles

- use React for rendering the HTML list and button ui
- use the Flux data flow pattern
- code must be unit tested
- use local JSON files as pages of articles to display
- all articles have unique ids, titles and images. one article is:

```
{
	id: 1,
	title: "article title 1",
	image: "http://placehold.it/300x250&text=image 1"
}
```
