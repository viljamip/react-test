/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import keyMirror from 'react/lib/keyMirror';

export default keyMirror({
  CHANGE_LOCATION: null,
  ARTICLE_CHANGE: null,
  ARTICLE_GET_MORE: null
});
