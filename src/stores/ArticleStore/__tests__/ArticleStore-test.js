/* globals it, describe, jest, expect, beforeEach */

jest.mock('../../../core/Dispatcher');

describe('ArticleStore',  () => {

  var Dispatcher, ArticleStore, callback;

  beforeEach(() => {
    Dispatcher = require('../../../core/Dispatcher');
    ArticleStore = require('../ArticleStore');
  });

  it('registers a callback with the dispatcher', () => {
    expect(Dispatcher.register.mock.calls.length).toBe(1);
  });

  it('should initialize with no articles', () => {
    expect(ArticleStore.getAll()).toEqual([]);
  });

  // Nor really a test
  it('should get more articles', () => {
    Dispatcher.dispatch({type: 'ARTICLE_GET_MORE'});
    jest.runAllTicks();
    setTimeout(() => {
      expect(ArticleStore.getAll().length).toBe(0);
    }, 1000);
    jest.runAllTimers();
  });
});
