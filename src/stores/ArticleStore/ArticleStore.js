
import Dispatcher from '../../core/Dispatcher';
import http from '../../core/http';
import ActionTypes from '../../constants/ActionTypes';
import EventEmitter from 'eventemitter3';

var LEAP = 1;
var articles = [];
var latestArticle = 1;

var getMore = function(){
  var from = latestArticle;
  var to = from + LEAP;
  return http.get('/api/articles?from=' + from + '&to=' + to + '.json')
  .then(function(newArticles){
    latestArticle = to + 1;
    articles = articles.concat(newArticles);
    return newArticles;
  });
};

class ArticleStore extends EventEmitter {
  getAll () {
    return articles;
  }

  emitChange () {
    this.emit(ActionTypes.ARTICLE_CHANGE);
  }

  addChangeListener (callback) {
    this.on(ActionTypes.ARTICLE_CHANGE, callback);
  }

  removeChangeListener (callback) {
    this.removeListener(ActionTypes.ARTICLE_CHANGE, callback);
  }

}

var articleStore = new ArticleStore();

var actions = {};
actions[ActionTypes.ARTICLE_GET_MORE] = getMore;

var dispatcherIndex = Dispatcher.register(function (action){
  console.log('asdfsadadsfasfdasdfasdf')
  var fn = actions[action.type];
  if (fn){
    fn(action.data);
    articleStore.emitChange();
  }
});

export default articleStore;
