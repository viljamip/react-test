var templateArticle = {
  id: 0,
  title: 'article title ',
  image: 'http://placehold.it/300x250&text=image '
};

var createArticle = function(o, n){
  return Object
  .keys(o)
  .reduce(function(a, k){
    a[k] = o[k] + n;
    return a;
  }, {});
};

export default {
  get (req, res, next) {
    var from = parseInt(req.query.from || 1, 10);
    var to = parseInt(req.query.to || 1, 10);
    from = from < 1 ? 1 : from;
    to = to < from ? from : to;

    var result = [];
    for (var i = from; i <= to; i++){
      result.push(createArticle(templateArticle, i));
    }

    res.json(result);
  }
};
