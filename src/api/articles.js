import { Router } from 'express';
import articleHandler from './articleHandler';

const router = new Router();

router.get('/', articleHandler.get);

export default router;
