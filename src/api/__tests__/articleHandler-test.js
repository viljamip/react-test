/* globals jest, describe, it */

jest.dontMock('../articleHandler');
jest.dontMock('express');


describe('Articles API', function() {

  var articleHandler = require('../articleHandler');

  it('should be defined', () => {
    expect(articleHandler).toBeDefined();
  });

  it('default response is array with one article', () => {
    articleHandler.get({query: {}}, {
      json: a => {
        expect(a.length).toEqual(1);
      }
    });
  });

  it('should response one article if to query parameter is not defined', () => {
    articleHandler.get({query: {from: 2}}, {
      json: a => {
        expect(a.length).toEqual(1);
      }
    });
  });


  it('should return articles between from and to query parameters', () => {
    articleHandler.get({query: {from: 5, to: 7}}, {
      json: a => {
        expect(a.length).toEqual(3);
      }
    });
  });

  it('should not accept to be smaller than from', () => {
    articleHandler.get({query: {from: 5, to: 4}}, {
      json: a => {
        expect(a.length).toEqual(1);
      }
    });
  });

  it('should not accept to be from being smaller than 1', () => {
    articleHandler.get({query: {from: -1, to: 4}}, {
      json: a => {
        expect(a.length).toEqual(4);
      }
    });
  });
});
