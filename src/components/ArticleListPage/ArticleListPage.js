import React, { Component } from 'react';
import styles from './ArticleListPage.css';
import ArticleStore from '../../stores/ArticleStore';
import ArticleList from '../ArticleList';
import ArticleActions from '../../actions/ArticleActions';
import withStyles from '../../decorators/withStyles';

@withStyles(styles)
class ArticleListPage extends Component {

  state = {
    articles: []
  }

  constructor (props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  componentDidMount () {
    ArticleStore.addChangeListener(this.onChange);
  }

  componentWillUnmount () {
    ArticleStore.removeChangeListener(this.onChange);
  }

  render () {
    return (
      <div className="ArticleListPage">
        <div className="container">
          <h1>Articles</h1>
          <ArticleList data={this.state.articles || []}></ArticleList>
          <button onClick={this.getMoreArticles}>Get More Articles</button>
        </div>
      </div>
    );
  }

  onChange () {
    this.setState({articles: ArticleStore.getAll()});
  }

  getMoreArticles (){
    ArticleActions.getMore();
  }
}

export default ArticleListPage;
