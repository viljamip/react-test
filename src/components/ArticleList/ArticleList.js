/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React from 'react';
import styles from './ArticleList.css';
import withStyles from '../../decorators/withStyles';
import Article from '../Article';

@withStyles(styles)
class ArticleList {
  render() {
    var articleNodes = this.props.data.map(function (article, key) {
      return (
        <Article data={article} key={key}></Article>
      );
    });

    return (
      <div className="articleList">
        {articleNodes}
      </div>
    );
  }
}

export default ArticleList;
