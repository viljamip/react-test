/*! React Starter Kit | MIT License | http://www.reactstarterkit.com/ */

import React from 'react';
import styles from './Article.css';
import withStyles from '../../decorators/withStyles';

@withStyles(styles)
class Article {
  render() {
    return (
      <div className="article">
        <h1>{this.props.data.title}</h1>
        <img src={this.props.data.image} />
      </div>
    );
  }
}

export default Article;
