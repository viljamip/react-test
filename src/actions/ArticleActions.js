var Dispatcher = require('../core/Dispatcher');
var ActionTypes = require('../constants/ActionTypes');

export default {
  getMore (data) {
    data = ! data ? {} : data;
    console.log('article Action - get more');
    Dispatcher.dispatch({
      type: ActionTypes.ARTICLE_GET_MORE,
      from: data.from || -1,
      to: data.to || -1
    });
  }
};
